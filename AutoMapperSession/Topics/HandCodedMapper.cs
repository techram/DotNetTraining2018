﻿using AutoMapperSession.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMapperSession.Topics
{
    public class HandCodedMapper
    {
        public static void Map()
        {
            var customer = new Customer
            {
                Name = "George Costanza"
            };
            var order = new Order
            {
                Customer = customer
            };
            var bosco = new Product
            {
                Name = "Bosco",
                Price = 4.99m
            };
            order.AddOrderLineItem(bosco, 15);

            var dto = new OrderDTO();

            dto.CustomerName = order.Customer.Name;
            dto.Total = order.GetTotal();

            Console.WriteLine("Customer Name: " + dto.CustomerName);
            Console.WriteLine("Orer Total:" + dto.Total);
        }
    }
}
