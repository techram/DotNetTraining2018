﻿using AutoMapperSession.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMapperSession
{
    class Program
    {
        static void Main(string[] args)
        {
            //HandCodedMapper.Map();

            //AutoMapperBasics.SimpleMap();
            //AutoMapperBasics.MapFrom();
            //AutoMapperBasics.ReverseMap();
            //AutoMapperBasics.Ignore();
            //AutoMapperBasics.ConditionalMapping();
            //AutoMapperBasics.GenericMapping();
            AutoMapperBasics.OpenGenericMapping();
            Console.ReadLine();
        }
    }
}
