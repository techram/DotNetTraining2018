﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMapperSession.Entities
{
    public class SampleList<T>
    {
        public T Value { get; set; }
        public string Name { get; set; }
    }
}
