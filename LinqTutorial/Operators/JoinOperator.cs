﻿using LinqSession.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSession.Operators
{
    public static class JoinOperator
    {
        public static void InnerJoin()
        {
            using (var db = new Northwind())
            {
                var query = from c in db.Categories
                            join p in db.Products on c.CategoryID equals p.CategoryID
                            select new { Category = c, p.ProductName };


                foreach (var item in query)
                {
                    Console.WriteLine("Name : {0} - {1}", item.Category.CategoryName, item.ProductName);
                }
            }
        }

        public static void GroupJoin()
        {
            using (var db = new Northwind())
            {
                var query = from c in db.Categories
                            join p in db.Products on c.CategoryID equals p.CategoryID into ps
                            select new { Category = c, Products = ps, count = ps.Count() };


                foreach (var item in query)
                {
                    Console.WriteLine("Name : {0} - {1}", item.Category.CategoryName, item.count);

                    foreach (var product in item.Products)
                    {
                        Console.WriteLine("Product Name : {0}", product.ProductName);
                    }

                    Console.WriteLine("==================");
                }
            }
        }

        public static void InnerJoinWithGroupJoin()
        {
            using (var db = new Northwind())
            {
                var query = from c in db.Categories
                            join p in db.Products on c.CategoryID equals p.CategoryID into ps
                            from p in ps
                            select new { Category = c, p.ProductName };

                foreach (var item in query)
                {
                    Console.WriteLine("Name : {0} - {1}", item.Category.CategoryName, item.ProductName);
                }
            }
        }


        public static void LeftOuterJoin()
        {

            using (var db = new Northwind())
            {
                var query = from c in db.Categories
                            join p in db.Products on c.CategoryID equals p.CategoryID into ps
                            from p in ps.DefaultIfEmpty()
                            where p == null
                            select new { Category = c, ProductName = p == null ? "(No products)" : p.ProductName };

                foreach (var item in query)
                {
                    Console.WriteLine("Name : {0} - {1}", item.Category.CategoryName, item.ProductName);
                }
            }
        }

    }
}
