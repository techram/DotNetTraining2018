﻿using LinqSession.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSession.Operators
{
    public static class WhereOperator
    {
        public static void Simple()
        {
            using (var db = new Northwind())
            {
                var soldOutProducts = from p in db.Products
                                      where p.UnitsInStock == 0
                                      select p;

                Console.WriteLine("Sold out products:");
                foreach (var product in soldOutProducts)
                {
                    Console.WriteLine("{0} is sold out!", product.ProductName);
                }

            }
        }

        public static void DrillDown()
        {
            using (var db = new Northwind())
            {
                var waCustomers = from c in db.Customers
                                  where c.Region == "WA"
                                  select c;

                Console.WriteLine("Customers from Washington and their orders:");
                foreach (var customer in waCustomers)
                {
                    Console.WriteLine("Customer {0}: {1}", customer.CustomerID, customer.CompanyName);
                    foreach (var order in customer.Orders)
                    {
                        Console.WriteLine("  Order {0}: {1}", order.OrderID, order.OrderDate);
                    }
                }

            }
        }

        public static void Any()
        {
            using (var db = new Northwind())
            {
                var anyOrder = db.Customers.Where(x => x.Orders.Any());

                Console.WriteLine("Customers with order");

                Console.WriteLine("Count : {0}", anyOrder.Count());

                Console.ReadLine();

                foreach (var customer in anyOrder)
                {
                    Console.WriteLine("Customer {0}: {1}", customer.CustomerID, customer.CompanyName);
                    foreach (var order in customer.Orders)
                    {
                        Console.WriteLine("  Order {0}: {1}", order.OrderID, order.OrderDate);
                    }
                }

            }
        }

        public static void AnyWithCondition()
        {
            using (var db = new Northwind())
            {
                var anyOrder = db.Customers
                        .Where(x => x.Orders.Any(y => y.Order_Details
                          .Sum(z => z.Quantity * z.Product.UnitPrice) > 9000));

                Console.WriteLine("Customers with order value >9000");

                Console.WriteLine("Count : {0}", anyOrder.Count());

                Console.ReadLine();
                foreach (var customer in anyOrder)
                {
                    Console.WriteLine("Customer {0}: {1}", customer.CustomerID, customer.CompanyName);
                    foreach (var order in customer.Orders)
                    {
                        Console.WriteLine("  Order {0}: {1}", order.OrderID, order.Order_Details.Sum(z => z.Quantity * z.Product.UnitPrice));
                    }
                }

            }
        }

        public static void AllWithCondition()
        {
            using (var db = new Northwind())
            {
                var anyOrder = db.Customers
                        .Where(x => x.Orders.All(y => y.Order_Details
                          .Sum(z => z.Quantity * z.Product.UnitPrice) > 9000));

                Console.WriteLine("Customers with order value >9000");

                Console.WriteLine("Count : {0}", anyOrder.Count());

                Console.ReadLine();
                foreach (var customer in anyOrder)
                {
                    Console.WriteLine("Customer {0}: {1}", customer.CustomerID, customer.CompanyName);
                    foreach (var order in customer.Orders)
                    {
                        Console.WriteLine("  Order {0}: {1}", order.OrderID, order.Order_Details.Sum(z => z.Quantity * z.Product.UnitPrice));
                    }
                }

            }
        }
    }
}
