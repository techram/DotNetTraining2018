﻿using LinqSession.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSession.Operators
{
    public static class ProjectionOperators
    {
        public static void Simpe()
        {
            using (var db = new Northwind())
            {
                var productNames = from p in db.Products
                                   select p.ProductName;

                Console.WriteLine("Product Names:");
                foreach (var productName in productNames)
                {
                    Console.WriteLine(productName);
                }
            }
        }

        public static void AnonymousType()
        {
            using (var db = new Northwind())
            {
                var productInfos = from p in db.Products
                                   select new { p.ProductName, p.Category, Price = p.UnitPrice };

                Console.WriteLine("Product Info:");
                foreach (var productInfo in productInfos)
                {
                    Console.WriteLine("{0} is in the category {1} and costs {2} per unit.", productInfo.ProductName, productInfo.Category.CategoryName, productInfo.Price);
                }
            }
        }

        public static void StronglyTyped()
        {
            using (var db = new Northwind())
            {
                var productInfos = from p in db.Products
                                   select new ProductDetail { ProductName = p.ProductName, Category = p.Category, Price = p.UnitPrice };

                Console.WriteLine("Product Info:");
                foreach (var productInfo in productInfos)
                {
                    Console.WriteLine("{0} is in the category {1} and costs {2} per unit.", productInfo.ProductName, productInfo.Category.CategoryName, productInfo.Price);
                }
            }
        }

        public static void SelectMany()
        {
            using (var db = new Northwind())
            {

                var anyOrder = db.Customers
                    .SelectMany(x => x.Orders
                                    .Where(y => y.Order_Details
                         .Sum(z => z.Quantity * z.Product.UnitPrice) > 9000));

                Console.WriteLine("Customers with order value >9000");

                Console.WriteLine("Count : {0}", anyOrder.Count());

                Console.ReadLine();
                foreach (var order in anyOrder)
                {
                    Console.WriteLine("Order {0}: {1}", order.CustomerID, order.OrderDate);
                    foreach (var orderDetail in order.Order_Details)
                    {
                        Console.WriteLine("  Order Detail {0}: {1}", orderDetail.Quantity, orderDetail.UnitPrice);
                    }
                }

            }
        }

        public static void MultiSelectMany()
        {
            using (var db = new Northwind())
            {

                var anyOrder = db.Customers
                    .SelectMany(x => x.Orders
                                    .Where(y => y.Order_Details
                         .Sum(z => z.Quantity * z.Product.UnitPrice) > 9000)
                         .SelectMany(od => od.Order_Details)).AsEnumerable();

                Console.WriteLine("Customers with order value >9000");

                Console.WriteLine("Count : {0}", anyOrder.Count());

                Console.ReadLine();
                foreach (var orderDetail in anyOrder)
                {

                    Console.WriteLine("  Order Detail {0}: {1}", orderDetail.Quantity, orderDetail.UnitPrice);

                }

            }
        }

        public static void ToDictionary()
        {
            using (var db = new Northwind())
            {
                var anyOrder = db.Customers
                                 .Where(x => x.Orders.Any())
                                 .ToDictionary(x => x.CustomerID);

                Console.WriteLine("Customers with order");

                Console.WriteLine("Count : {0}", anyOrder.Count());

                Console.ReadLine();

                foreach (var customer in anyOrder)
                {
                    Console.WriteLine("Customer {0}", customer);
                    foreach (var order in anyOrder[customer.Key].Orders)
                    {
                        Console.WriteLine("  Order {0}: {1}", order.OrderID, order.OrderDate);
                    }
                }

            }
        }

        public static void Concat()
        {
            using (var db = new Northwind())
            {
                var customerNames = from c in db.Customers
                                    select c.CompanyName;
                var productNames = from p in db.Products
                                   select p.ProductName;

                var allNames = customerNames.Concat(productNames);

                foreach (var item in allNames)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }
        }

        public static void FirstOrDefault()
        {
            using (var db = new Northwind())
            {
                Product product12 = (from p in db.Products
                                     where p.ProductID == 72394234
                                     select p)
                                    .FirstOrDefault();

                Console.WriteLine("Name : {0}", product12.ProductID);

                //var name = (from p in db.Products
                //                     where p.ProductID == 72394234
                //                     select p.UnitPrice)
                //                    .FirstOrDefault();

                //Console.WriteLine("Name : {0}", name    );
            }
        }

        public static void FirstOrderDefaultWithCondition()
        {
            using (var db = new Northwind())
            {
                Product product789 = db.Products
                                        .FirstOrDefault(p => p.ProductID == 12);

                Console.WriteLine("Name : {0}", product789.ProductName);
            }
        }

        //public void Different()
        //{
        //      using (var db = new Northwind())
        //    {
        //        var orders = db.Orders.AsQueryable();//.ToList();

        //        orders = orders.Where(x => x.Customer.CompanyName == "asdasdasd");

        //        var filterOrder = orders.Where(x => x.OrderID == 12).ToList();
        //    }
        //}
    }

    public class ProductDetail
    {
        public string ProductName { get; set; }
        public Category Category { get; set; }
        public decimal? Price { get; set; }
    }
}
